# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 11:25:25 2020
@author: nz
"""

from environment import Env
from agent import Agent
import math
import networkx as nx
import datetime
import tensorflow as tf
import matplotlib.pyplot as plt
import os
import numpy as np

def greedy_mvc(graph):
    G = graph.copy()
    cover_set = []
    while G.number_of_edges()>0:
        G = nx.convert_node_labels_to_integers(G,first_label = 0)
        #print('nodes',G.nodes())
        d = list(dict(G.degree(G.nodes)).values())
        #print('degrees',d)
        node = np.argmax(d)
        #print('node', node)
        G.remove_node(node)
        cover_set.append(node)
    return len(cover_set)


if __name__=="__main__":
    embedding_dim =64
    graph_size = 10
    num_episodes = 400
    replay_steps = 5
    batch_size = 128
    Training = False
    Testing = True
    env = Env(graph_size, embedding_dim)
    agent = Agent(graph_size, embedding_dim, batch_size = batch_size)
    if Training: 
        current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        train_log_dir = 'logs/gradient_tape/' + current_time + '/train'
        train_summary_writer = tf.summary.create_file_writer(train_log_dir)
        agent.manager = tf.train.CheckpointManager(agent.ckpt, train_log_dir+'/ckpt', max_to_keep = 3)
        fig_dir='./figures/'+current_time+'/'
        os.makedirs(fig_dir)
        plt.figure(figsize=(8, 6))
        for episode in range(0,num_episodes):
            graph = env.reset()
            pos = nx.spring_layout(graph)
            step = 0
            nsteps = 0
            fig_path_episode = fig_dir+ 'episode' + str(episode)+'/'
            os.makedirs(fig_path_episode)
            node_colors =['black' for i in range(graph.number_of_nodes())]
            while not graph.graph['done']:
                action,chosen_random = agent.choose_action(graph)
                if chosen_random:
                    node_colors[action]='orange'
                else:
                    node_colors[action]='green'
                graph = env.step(action)
                if step<replay_steps and nsteps>0:
                    agent.remember(graph, True)
                    step+=1
                    reset = False
                else:
                    agent.remember(graph,  False)
                    step =0
                    reset = True
                nsteps+=1
                gradients,loss,variables= agent.learn(reset)
                graph = env.make_transition()
                agent.eps= agent.min_eps + (agent.max_eps - agent.min_eps)*math.exp(-agent.lambda_ * episode)
                #print('eps value', agent.eps)
                edge_colors=['red' if graph.nodes[u]['tag'][0]== 1 or graph.nodes[v]['tag'][0]== 1 else 'black' for (u,v) in graph.edges]
                nx.draw(graph,pos=pos, node_size=20, node_color=node_colors,edge_color=edge_colors,hold=True)
                plt.title('Graph vizualisation', size=15)
                fig_path_step = fig_path_episode+'step'+ str(nsteps)+'.png'
                plt.savefig(fig_path_step)
                plt.clf()
            #saving checkpoints
            agent.ckpt.step.assign_add(1)
            agent.manager.save()
            #print('episode',episode)
            #print('loss',loss)
            with train_summary_writer.as_default():
                tf.summary.scalar('loss', loss, step=episode)
                for i,gradient in enumerate(gradients):
                    tf.summary.histogram('gradient' + str(i), gradient, step=episode)
                for i,variable in enumerate(gradients):
                    tf.summary.histogram('variable' + str(i), variable, step=episode)
    elif Testing:
        graph_size = 10
        env = Env(graph_size, embedding_dim)
        agent = Agent(graph_size, embedding_dim, batch_size = batch_size)
        test_ckpt_path = './logs/gradient_tape/20200225-130753/train/ckpt'
        agent.manager = tf.train.CheckpointManager(agent.ckpt, test_ckpt_path, max_to_keep = 3)
        agent.ckpt.restore(agent.manager.latest_checkpoint)
        agent.eps = 0
        test_num = 50
        ratio = 0
        for test_iter in range(0, test_num):
            graph = env.reset()
            while not graph.graph['done']:
                action,_=agent.choose_action(graph)
                graph = env.step(action)
                graph = env.make_transition()
            agent_covered_nodes = int(sum(list(nx.get_node_attributes(graph,'tag').values()))[0])
            print('agent covered ', agent_covered_nodes, ' nodes')
            greedy_covered_nodes = greedy_mvc(graph)
            print('greedy covered ', greedy_covered_nodes, ' nodes')
            ratio += max(agent_covered_nodes/greedy_covered_nodes, greedy_covered_nodes/agent_covered_nodes)
        approximation_ratio = ratio/test_num
        print(approximation_ratio)
            
            