from qfunction import Q
from replay_buffer import Memory
import numpy as np
from numpy import random
import networkx as nx
import tensorflow as tf
from tensorflow import keras
import scipy as sp

class Agent:
    def __init__(self, graph_size, embedding_dim, batch_size=128,
                 max_eps=1, min_eps=0.01,
                 training = True,
                 gamma=0.99, lambda_ = 0.009,
                 T=5,
                 max_memory=100000):
        self.graph_size = graph_size
        self.max_eps = max_eps
        self.min_eps = min_eps
        self.eps = max_eps
        self.training =training
        self.gamma = gamma
        self.lambda_ = lambda_
        self.batch_size = batch_size
        self.embedding_dim = embedding_dim
        self.T = T
        self.Q = Q(self.embedding_dim, self.T)
        self.Q_hat = Q(self.embedding_dim, self.T)
        self.optimizer = tf.keras.optimizers.Adam(0.01)
        self.ckpt = tf.train.Checkpoint(step=tf.Variable(1), optimizer = self.optimizer, net = self.Q)
        self.manager = tf.train.CheckpointManager(self.ckpt, './tf_ckpt', max_to_keep = 3)
        self.memory = Memory(max_memory,self.graph_size,self.embedding_dim)
        
    def choose_action(self, graph):
        #exploration phase: if a random number is less than the _eps value, choose random node
        if random.random() < self.eps:
            #choose a random action from the set of possible actions
            action = random.choice(graph.nodes)
            #chosen node must not belong to the current partial solution
            while (graph.nodes[int(action)]['tag'][0]==1):
                action = random.choice(graph.nodes)
            chosen_random=True
            return int(action),chosen_random
        #exploitation phase: choose node based on network output
        else:
            #choose the action for which the Q function is max: choose maximum of the network output
            #action with highest Q has highest expected current+future discounted reward
            #the chosen action must not belong to the actual partial solution
            predictions = self.Q(graph,batch_size=1,state='tag',training = False)
            predictions = [predictions[i] if graph.nodes[i]['tag'][0]==0 else np.nan for i in np.arange(self.graph_size)]
            action = sp.nanargmax(predictions)
            chosen_random=False
            return int(action), chosen_random

    def remember(self, graph,delay_reward):
        self.memory.add_sample(graph,delay_reward) 
    
    def learn(self,reset):
        if self.memory._memory_counter < self.batch_size:
            batch_size = self.memory._memory_counter
        else:
            batch_size = self.batch_size
        graph_batch = self.memory.draw_sample(batch_size)     
        action = graph_batch.graph['action']
        done = graph_batch.graph['done']
        reward_sum = graph_batch.graph['reward']
        if reset == True:
            for i in range(0,len(self.Q.trainable_variables)):
                self.Q_hat.trainable_variables[i].assign(self.Q.trainable_variables[i])
        q_target = self.Q_hat(graph_batch, batch_size, state='new tag', training=False)
        y_target = reward_sum/self.graph_size + (1-done)*self.gamma * tf.math.reduce_max(q_target,axis=0)
        with tf.GradientTape() as tape:
            #extract from tensor of shape (num_nodes,batch_size) the q_value of action(node) corresponding to batch element
            #returns tensor of shape (batch_size)
            q_pred = self.Q(graph_batch, batch_size, state = 'tag', training=True)
            if len(action)<2:
                y_pred = tf.gather(q_pred,action)
            else:
                y_pred = tf.gather_nd(q_pred, np.column_stack((action,np.arange(0,batch_size))))
            loss =tf.reduce_mean( tf.losses.mean_squared_error(y_target,y_pred))
            variables = self.Q.trainable_variables
        gradients = tape.gradient(loss,variables)
        gradients = [grad if grad is not None else tf.zeros_like(var) for var, grad in zip(variables, gradients)]
        self.optimizer.apply_gradients(zip(gradients, variables))
        return gradients,loss,variables

    