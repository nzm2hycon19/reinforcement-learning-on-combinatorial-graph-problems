# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 11:26:53 2020

@author: nz
"""

import networkx as nx
import numpy as np
import random
from environment import Env

class Memory:
    def __init__(self,max_memory,graph_size,embedding_size):
        self._max_memory=max_memory
        self.env= Env(graph_size,embedding_size)
        self.memory_graph = self.env.reset()
        for i,d in enumerate(self.memory_graph.graph):
            self.memory_graph.graph[d]=[]
            self.memory_graph.graph[d].append(0)
        self._memory_counter = 0
        
    def add_sample(self, graph, delay_reward):
        
        if delay_reward ==True:
            self.memory_graph.graph['reward'][-1]+=graph.graph['reward']
            self.memory_graph.graph['done'][-1]=graph.graph['done']
            memory_new_tags = np.array(list(nx.get_node_attributes(self.memory_graph,'new tag').values()))
            graph_new_tags = np.array(list(nx.get_node_attributes(graph,'new tag').values()))
            memory_new_tags[:,[-1]] =graph_new_tags
            node_tags = dict(zip(self.memory_graph.nodes,memory_new_tags))
            nx.set_node_attributes(self.memory_graph,node_tags,'new tag')
            
            memory_embeddings = np.array(list(nx.get_node_attributes(self.memory_graph,'node_embedding').values()))
            graph_embeddings = np.array(list(nx.get_node_attributes(graph,'node_embedding').values()))
            memory_embeddings [:,[-1],:]= graph_embeddings
            node_embeddings = dict(zip(self.memory_graph.nodes,memory_embeddings))
            nx.set_node_attributes(self.memory_graph,node_embeddings,'node_embedding')
        else:    
            #add done, cost, action, reward information
            for i,d in enumerate(graph.graph):
                self.memory_graph.graph[d].append(graph.graph[d])
            #adding tags, has shape (num_nodes,num_samples)
            memory_tags = np.array(list(nx.get_node_attributes(self.memory_graph,'tag').values()))
            graph_tags = np.array(list(nx.get_node_attributes(graph,'tag').values()))
            memory_tags=np.append(memory_tags,graph_tags, axis= 1)
            node_tags = dict(zip(self.memory_graph.nodes,memory_tags))
            nx.set_node_attributes(self.memory_graph,node_tags,'tag')
            
            #adding tags for the new state, has shape (num_nodes,num_samples)
            memory_new_tags = np.array(list(nx.get_node_attributes(self.memory_graph,'new tag').values()))
            #print('memory_new_tags',memory_new_tags)
            graph_new_tags = np.array(list(nx.get_node_attributes(graph,'new tag').values()))
            #print('graph_new_tags',graph_new_tags)
            memory_new_tags=np.append(memory_new_tags,graph_new_tags, axis= 1)
            node_tags = dict(zip(self.memory_graph.nodes,memory_new_tags))
            nx.set_node_attributes(self.memory_graph,node_tags,'new tag')
            
            #adding embeddings, has shape (num_nodes,num_samples,embedding_size)
            memory_embeddings = np.array(list(nx.get_node_attributes(self.memory_graph,'node_embedding').values()))
            graph_embeddings = np.array(list(nx.get_node_attributes(graph,'node_embedding').values()))
            memory_embeddings = np.append(memory_embeddings,graph_embeddings, axis= 1)
            node_embeddings = dict(zip(self.memory_graph.nodes,memory_embeddings))
            nx.set_node_attributes(self.memory_graph,node_embeddings,'node_embedding')
            self._memory_counter +=1
        
    def draw_sample(self, num_samples):
        #drawing random indexes
        if num_samples>self._memory_counter:
            indexes = np.random.choice(self._memory_counter,size=self._memory_counter, replace = False)
        else:
            indexes = np.random.choice(self._memory_counter,size=num_samples, replace = False)
        #creating sample graph
        sample_graph = self.env.reset()
        #sampling graph attributes
        for i,d in enumerate(self.memory_graph.graph):
            sample_graph.graph[d] = np.array(self.memory_graph.graph[d])[indexes]
        #sampling node tags
        sample_tags = np.array(list(nx.get_node_attributes(self.memory_graph,'tag').values()))[:,indexes]
        nx.set_node_attributes(sample_graph,dict(zip(sample_graph.nodes,sample_tags)),'tag')
        #sampling node new tags (graph new state)
        sample_new_tags = np.array(list(nx.get_node_attributes(self.memory_graph,'new tag').values()))[:,indexes]
        nx.set_node_attributes(sample_graph,dict(zip(sample_graph.nodes,sample_new_tags)),'new tag')
        #sampling node embeddings
        sample_embeddings = np.array(list(nx.get_node_attributes(self.memory_graph,'node_embedding').values()))[:,indexes,:]
        nx.set_node_attributes(sample_graph,dict(zip(sample_graph.nodes,sample_embeddings)),'node_embedding')
        return sample_graph
        
        