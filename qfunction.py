# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 14:39:13 2020
@author: nz
"""
import networkx as nx
import tensorflow as tf
from tensorflow import keras
import numpy as np

class S2V(keras.Model):
    def __init__(self, embedding_dim):
        super(S2V, self).__init__()
        self.embedding_dim= embedding_dim
        tf.keras.backend.set_floatx('float64')
        self.dense1 = keras.layers.Dense(self.embedding_dim, kernel_initializer='random_uniform', use_bias = False, dtype = 'float64', name='layer1')
        self.dense2 = keras.layers.Dense(self.embedding_dim, kernel_initializer='random_uniform', use_bias = False, dtype = 'float64', name='layer2')
        self.dense3 = keras.layers.Dense(self.embedding_dim, kernel_initializer='random_uniform', use_bias = False,dtype = 'float64', name='layer3')
        self.dense4 = keras.layers.Dense(self.embedding_dim, kernel_initializer='random_uniform', activation=tf.nn.relu, use_bias = False, dtype='float64', name='layer4')
    
    def call(self, graph, batch_size,state ='tag', training = False):
        if batch_size ==1:
            
            node_embeddings = np.array(list(nx.get_node_attributes(graph,'node_embedding').values()))
            aggregate_embeddings = np.zeros_like(node_embeddings)
            x = []
            aggregate_weights = []
            for i in range(len(node_embeddings)):
                x.append(self.dense1(np.array(graph.nodes[i][state]).reshape(1,1)))
                aggregate_embeddings[i] = sum(node_embeddings[list(nx.all_neighbors(graph,i))])            
                s = 0
                for j in (list(nx.all_neighbors(graph,i))):
                    s += self.dense4(np.array(graph.edges[i,j]['weight']).reshape(1,1))
                aggregate_weights.append(s)
                
            aggregate_embeddings = self.dense2(aggregate_embeddings)
            aggregate_weights = self.dense3(np.array(aggregate_weights))
            return tf.squeeze(tf.nn.relu(x + aggregate_embeddings + aggregate_weights))
        else:
            self.batch_size = batch_size
            node_embeddings = np.array(list(nx.get_node_attributes(graph,'node_embedding').values()))
            aggregate_embeddings = np.zeros_like(node_embeddings)
            
            x = []
            aggregate_weights = []
            for i in range(len(node_embeddings)):
                x.append(self.dense1(np.array(graph.nodes[i][state]).reshape(self.batch_size,1)))
                aggregate_embeddings[i]=sum(node_embeddings[list(nx.all_neighbors(graph,i))])            
                s = 0
                for j in (list(nx.all_neighbors(graph,i))):
                    s += self.dense4(np.array(graph.edges[i,j]['weight']).reshape(1,1))
                aggregate_weights.append(np.repeat(s,self.batch_size,axis=0))
                
            aggregate_embeddings = self.dense2(aggregate_embeddings)
            aggregate_weights = self.dense3(np.array(aggregate_weights))
            return tf.nn.relu(x + aggregate_embeddings + aggregate_weights)
    
class Q(keras.Model):
    def __init__(self, embedding_dim,T):
        super(Q, self).__init__()
        self.embedding_dim = embedding_dim
        self.T = T
        tf.keras.backend.set_floatx('float64')
        self.dense5 = keras.layers.Dense(1, kernel_initializer='random_uniform', use_bias = False, dtype = 'float64', name='layer5')
        self.dense6 = keras.layers.Dense(self.embedding_dim, kernel_initializer='random_uniform', use_bias = False, dtype = 'float64', name='layer6')
        self.dense7 = keras.layers.Dense(self.embedding_dim, kernel_initializer='random_uniform', use_bias = False, dtype = 'float64', name='layer7')
        self.s2v = S2V(self.embedding_dim)
        
    def call(self, graph, batch_size, state='tag', training = False):
        if batch_size ==1:
            #compute embeddings until final iteration
            for i in range(self.T):
                #embeddings mu are stuct2vecs
                mu = self.s2v(graph,1,state)
                #store each newly computed embedding vector as an attribute to the corresponding node
                nx.set_node_attributes(graph, dict(zip(list(graph.nodes),tf.squeeze(mu).numpy())),'node embeddings')
            #sum over all node embeddings
            graph_pool = tf.math.reduce_sum(mu, axis = 0, keepdims = True)
            graph_pool = self.dense6(graph_pool)
            #initialize q function output
            q= []
            for i in range(graph.number_of_nodes()):
                node_vec = self.dense7(np.array(mu[i]).reshape(1,self.embedding_dim))
                concat = keras.layers.concatenate([graph_pool, node_vec], axis = 1)
                q.append(tf.squeeze(self.dense5(tf.nn.relu(concat))))
            return tf.squeeze(q)
        else:
            self.batch_size = batch_size
            #compute embeddings until final iteration
            for i in range(self.T):
                #embeddings mu are stuct2vecs
                mu = self.s2v(graph,batch_size,state)
                #store each newly computed embedding vector as an attribute to the corresponding node
                nx.set_node_attributes(graph, dict(zip(list(graph.nodes),mu.numpy())),'node embeddings')
            #sum over all node embeddings
            graph_pool = tf.math.reduce_sum(mu, axis = 0, keepdims = True)
            graph_pool = self.dense6(graph_pool)
            #initialize q function output
            q= []
            for i in range(graph.number_of_nodes()):
                node_vec = self.dense7(np.array(mu[i]).reshape(1,self.batch_size,self.embedding_dim))
                concat = keras.layers.concatenate([graph_pool, node_vec], axis = 2)
                q.append(self.dense5(tf.nn.relu(concat)))
            return tf.squeeze(q)

