# Reinforcement learning on combinatorial graph problems
**environment.py**

`Env()`: Defines the environment for the Minimum Vertex cover problem and interaction of the agent.
- `__init__`: initialize the graph with the size and embedding dimension for nodes
- `reset`: 
    - Create a graph instance and initialize the weights of the edges.  
    - Initialize graph attributes `done` and `cost`.
    - Initialize node attributes:
        - `tag` (0 if node doesn't belong to current solution, 1 if it does)
        - `new tag` (0 if node doesn't belong to next solution, 1 if it does) 
        - `node_embedding` (A 1xembedding_dim vector that holds node embedding)
- `step`: Calculate the outcome of a taking the given action: 
    - Add the node to the next state.
    - Calculate the new cost ![img](http://latex.codecogs.com/svg.latex?%24c%28h%28S%27%29%2CG%29%3D-%5Ctext%7Bcard%7D%28S%27%29%24).
    - Calculate the reward ![img](http://latex.codecogs.com/svg.latex?%24r%28S%2Cv%29%3Dc%28h%28S%27%29%2CG%29-c%28h%28S%29%2CG%29%24).
    - Update graph tags for the new state: reward, cost, action and done flag.
- `make transition`: set next state as current state.
- `_BA`: Generate a Barabási–Albert graph, with attributes `done`, `cost`, `action`, `reward` that are going to be updated with each state.
- `_cover`: Count the number of edges covered.
- `_done`: Check if all edges are covered (valued 1.0 or 0.0)

**qfunction.py**

`S2V()`: Neural Network model that calculates node embeddings:

![img](http://latex.codecogs.com/svg.latex?%24%5Cmu_v%5E%7B%28t%2B1%29%7D%5Cleftarrow+%5Ctext%7Brelu%7D%28%5CTheta_1x_v%2B%5CTheta_2%5Csum_%7Bu%5CinN%28v%29%7D%5Cmu_u%5E%7B%28t%29%7D%2B%5CTheta_3%5Csum_%7Bu%5CinN%28v%29%7D%5Ctext%7Brelu%7D%28%5CTheta_4w%28v%2Cu%29%29%29%24).
- ![img](http://latex.codecogs.com/svg.latex?%24%5Cmu_v%5E%7B%28t%29%7D%24) is the node embedding of node v at time step t.
- ![img](http://latex.codecogs.com/svg.latex?%24x_v%24) is the node tag
- ![img](http://latex.codecogs.com/svg.latex?%24%5CTheta_i%24) are the model parameters (weights).

`Q()` : Neural Network model that calculates the q value for each node in the given graph:
![img](http://latex.codecogs.com/svg.latex?%24Q%28h%28S%29%2Cv%2C%5CTheta%29%3D%5CTheta_5%5ET%5Ctext%7Brelu%7D%28%5B%5CTheta_6%5Csum_%7Bu%5CinV%7D%5Cmu_v%5E%7B%28T%29%7D%2C%5CTheta_7%5Cmu_v%5E%7B%28T%29%7D%5D%29%24)

The node embeddings are computed and updated for T iterations then the Q function calculates for each node, based on its node embedding, and outputs its q value using the equation above.

**agent.py**

`Agent()`: A class that models the agent and its actions.

- `__init__`: Initialize the agent with all hyperparameters and learning parameters, additionally it has a memory.
- `choose_action`: Here the action is chosen following an epsilon-greedy policy: at the first stages we want the agent to explore the environment, so we encourage
 the agent to take random action, at later stages we want the agent to exploit what it learnt, so we choose the action which has the maximum q value.
 In both cases the chosen action must not belong to the current solution. Which stratecy to follow is defined by the parameter epsilon which is set
high for the first episodes and exponantially decays.
- `remember`: Store a transition in memory.
- `learn`: defines the training procedure for the agent:
    - ![img](http://latex.codecogs.com/svg.latex?%24y_%7Btarget%7D%3D%5Csum_%7Bi%3D0%7D%5E%7Bn-1%7Dr%28S_%7Bt%2Bi%7D%2Cv_%7Bt%2Bi%7D%29%2B%5Cgamma%5Cmax_%7Bv%27%7D%5Chat%7BQ%7D%28h%28S_%7Bt%2Bn%7D%29%2Cv%27%2C%5CTheta%5E-%29%24)
        Where we include delayed reward, so that the agent has information about the future possible reward in the next n-steps. The parameters
        for the target are updated each n steps with the parameters of the Q function. 
    - ![img](http://latex.codecogs.com/svg.latex?%24y_%7Bpred%7D%3D%5Chat%7BQ%7D%28h%28S_%7Bt%7D%29%2Cv_t%2C%5CTheta%29%24) The q value of the given action.
    - ![img](http://latex.codecogs.com/svg.latex?%24loss%3DMSE%28y_%7Btarget%7D-y_%7Bpred%7D%29%24)
    - Parameters are updated using ADAM.

**replay_buffer.py**

`Memory`: Class that stores and samples transitions for batch training.

- `__init__`: initialize the memory, which is a graph with the same structure as the environment used. Each graph attribute is an array where the 
attributes of the graph we want to store are appended to. Likewise each node will contain a matrix of node embeddings that are added by the graph to be stored. same holds for states.
If we have n_samples then the memory graph will hold attributes of following shapes:
    - `done`,`cost`,`action`,`reward`: (num_samples,)
    - `tag`, `new_tag`: (num_nodes , num_samples)
    - `node_embedding`:  (num_nodes,num_samples,embedding_size)
- `add_sample`: Adds samples to the memory with the delayed reward option. 
    - If `delay_reward == False` then store transition ![img](http://latex.codecogs.com/svg.latex?%24%28S_t%2Cv_t%2Cr_t%2CS_%7Bt%2B1%7D%29%24).
    - If `delay_reward == True` then edit last stored transition as ![img](http://latex.codecogs.com/svg.latex?%24%28S_t%2Cv_t%2Cr_t%2Br_%7Bt%2B1%7D%2CS_%7Bt%2B2%7D%29%24).
- `draw_sample`: Return a graph whose attributes are a random sample from the memory graph.