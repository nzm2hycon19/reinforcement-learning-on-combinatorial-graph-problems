import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

#building the environment for minimum vertex cover
class Env():
    def __init__(self, graph_size,embedding_size):
        #number of nodes
        self.graph_size = graph_size
        self.embedding_size = embedding_size
        self.name = "MVC"
        self.reset()
        
    #regenerate and initialize graph
    def reset(self):
        #create graph
        self.graph = self._BA(self.graph_size)
        #initialize weights
        weights=np.random.random(self.graph.number_of_edges())
        edge_weights = dict(zip(list(self.graph.edges),weights))
        #assign weights to graph edges
        nx.set_edge_attributes(self.graph, edge_weights, 'weight')
        #assign node tags x_v: binary variable
        tags=np.zeros((nx.number_of_nodes(self.graph),1), dtype = np.float64)
        node_tags = dict(zip(self.graph.nodes,tags))
        #this will hold current solution
        nx.set_node_attributes(self.graph,node_tags,'tag')
        #this will hold new solution
        nx.set_node_attributes(self.graph,node_tags,'new tag')
        self.graph.graph['done'] = self._done()
        #sum over covered nodes
        cost =-sum(list(nx.get_node_attributes(self.graph,'tag').values()))[0]
        self.graph.graph['cost'] = cost
        #assign/initialize embedding vector to nodes
        mu = np.zeros((nx.number_of_nodes(self.graph),1,self.embedding_size),dtype=np.float64)
        node_embeddings = dict(zip(self.graph.nodes,mu))
        nx.set_node_attributes(self.graph , node_embeddings,'node_embedding')
        return self.graph
    
    #calculate the outcomes of taking an action
    def step(self, action):
        assert action in range(self.graph.number_of_nodes())
        nx.set_node_attributes(self.graph, nx.get_node_attributes(self.graph,'tag'),'new tag')
        #add slected node to partial solution
        self.graph.nodes[action]['new tag']=np.ones((1),dtype =np.float64)
        #update cost
        next_cost = -sum(list(nx.get_node_attributes(self.graph,'new tag').values()))[0]
        #calculate reward
        reward = next_cost - self.graph.graph['cost']
        self.graph.graph['action']= action
        self.graph.graph['reward'] = reward
        self.graph.graph['cost'] = next_cost
        return self.graph
    
    def make_transition(self):
        nx.set_node_attributes(self.graph,nx.get_node_attributes(self.graph,'new tag'), 'tag' )
        self.graph.graph['done'] = self._done()
        return self.graph
    
    def _BA(self, size, done=0.0, cost=0.0, action =0, reward = 0.0):
        #create a random graph which has attributes that hold the cost, chosen action, corresponding reward 
        #information and indicates if the task is done
        G =nx.Graph(nx.random_graphs.barabasi_albert_graph(n=size, m=6),
                    done = done, 
                    cost = cost,
                    action = action,
                    reward = reward)
        return G
    
    #count the number of edges covered by node tag
    def _cover(self):
        #edge is covered if one of its endpoints are tagged with 1 
        cover = np.array([1 if self.graph.nodes[u]['tag'][0]== 1 or self.graph.nodes[v]['tag'][0]== 1 else 0 for (u,v) in self.graph.edges])
        return sum(cover)
    
    #check if all edges are covered 
    def _done(self):
        if self._cover() == self.graph.number_of_edges():
            done_flag = 1.0
        else:
            done_flag = 0.0
        return done_flag


    
    